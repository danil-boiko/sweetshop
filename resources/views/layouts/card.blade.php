<div class="col-sm-6 col-md-4">
    <div class="thumbnail">
        <img src="https://cakeat.ru/images/recipe/2015/05/jd83fhw83fowwrt.jpg" alt="cake">
        <div class="caption">
            <h3>{{ $product->name }}</h3>
            <p>{{ $product->price }} руб.</p>
            <p>
                <form action="{{ route('basket-add', $product->id) }}" method="post">
                    <button type="submit" class="btn btn-primary" role="button">В корзину</button>
                    <a href="{{ route('product', [$product->category->code, $product->code]) }}"
                       class="btn btn-default" role="button">Подробнее</a>
                    @csrf
                </form>
            </p>
        </div>
    </div>
</div>
