@extends('layouts.master')

@section('title', 'Все категории')

@section('content')
    @foreach($categories as $category)
        <div class="panel">
            <a href="{{ route('category', $category->code) }}">
                <img src="https://avatars.mds.yandex.net/get-pdb/368827/6d9a7bf6-b9cc-4667-9472-ac98846790a6/s1200?webp=false">
                <h2>{{ $category->name }}</h2>
            </a>
            <p>{{ $category->description }}</p>
        </div>
    @endforeach
@endsection
